package karixTA.ApiHttpXml;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.LogStatus;

import karixTA.pages.ApiHttpXml;
import karixTA.support.ExtentReport;
import karixTA.support.LoungeLib;
import karixTA.support.RestAssured;

public class TestTC_API_007VerifyXMLPlainMessageForInterfaceRejection extends ApiHttpXml {
	@Test
	public void test() {
		LoungeLib loungelibObject = new LoungeLib();
		String Test_caseId="TC_API_007";
		loungelibObject.workBookName = "Api.xls";
		loungelibObject.workSheetName = "ApiData";
		loungelibObject.sNo = Test_caseId;
		loungelibObject.fetchTestData();
		testCaseID = "KRX-22";
		summary=testCaseID +" : Verify XML unicode message for less than 160 Characters";

		test = ExtentReport.extent.startTest(Test_caseId);
		try {
			//Create Object For RestAssured
			RestAssured restassuredObject = new RestAssured();
			
			//Post Xml Response
			message = incrementSteps() + "- Post Xml Response";
			restassuredObject.postXmlData(apiXMLUrl, loungelibObject.fileName);
			test.log(LogStatus.PASS, message + " -- executed successfully");
			
			//Get 19 digit mid should be generated for the request
			message = incrementSteps() + "- 19 digit mid should be generated for the request";
			String RequestID = restassuredObject.getRequestIdFromPostXmlResponse("requestId","xml");
			test.log(LogStatus.PASS, message + " -- executed successfully");
			
			//Get Stime from db
			message="RequestId - " + RequestID +" - is created at";
			fetchExcelData(loungelibObject, "S_time");
			String S_Time=getsTime(loungelibObject.selectQuery, RequestID);
			test.log(LogStatus.PASS, message +" - "+ S_Time);
			
			// check data inserted in sms_submissions
			fetchExcelData(loungelibObject,Test_caseId);
			message = incrementSteps() + "- One message should be inserted in sms_submissions";
			checkDataInsertedInDb(loungelibObject.executeQuery,RequestID);
			test.log(LogStatus.PASS, message + " -- executed successfully");
			
			fetchExcelData(loungelibObject, "TC_API_007_1");
			// check data inserted in sms_deliveries
			message = incrementSteps() + "- One message should be inserted in sms_deliveries";
			checkDataInsertedInDb(loungelibObject.executeQuery, RequestID);
			test.log(LogStatus.PASS, message + " -- executed successfully");
						
			fetchExcelData(loungelibObject, Test_caseId);
			//Verify status Flag and Status Data into Db
			message = incrementSteps() + "- Verify Status_flag and Status in 'billing.sms_submissions' is equals to "+loungelibObject.expectedResult ;
			Assert.assertTrue(verifyApiResponseIntoDB(loungelibObject, "status_flag,status",RequestID), message + " -- Failed");
			test.log(LogStatus.PASS, message + " -- executed successfully");
			
			fetchExcelData(loungelibObject, "TC_API_007_1");
			//Verify status Flag and Status Data into Db
			message = incrementSteps() + "- Verify PMS and PM in 'billing.sms_submissions equals to "+loungelibObject.expectedResult ;
			Assert.assertTrue(verifyApiResponseIntoDB(loungelibObject, "feature_cd,msg_class",RequestID), message + " -- Failed");
			test.log(LogStatus.PASS, message + " -- executed successfully");
			
			fetchExcelData(loungelibObject, "TC_API_007_2");
			//Verify status Flag and Status Data into Db
			message = incrementSteps() + "- Verify dest,cust_mid,tag1 in 'billing.sms_submissions' is equals to "+loungelibObject.expectedResult ;
			Assert.assertTrue(verifyApiResponseIntoDB(loungelibObject, "dest,cust_mid,tag1",RequestID), message + " -- Failed");
			test.log(LogStatus.PASS, message + " -- executed successfully");
			
			fetchExcelData(loungelibObject, "TC_API_007_3");
			//Verify status Flag and Status Data into Db
			message = incrementSteps() + "- Verify dest,cust_mid in 'billing.sms_deliveries' is equals to "+loungelibObject.expectedResult ;
			Assert.assertTrue(verifyApiResponseIntoDB(loungelibObject, "dest,cust_mid",RequestID), message + " -- Failed");
			test.log(LogStatus.PASS, message + " -- executed successfully");
			
		} catch (Exception e) {
			Assert.fail("Failed Test - VerifyXMLPlainMessageForInterfaceRejection");
		}
	}
}

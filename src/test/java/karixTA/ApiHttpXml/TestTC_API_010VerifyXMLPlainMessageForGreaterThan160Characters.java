package karixTA.ApiHttpXml;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.LogStatus;

import karixTA.pages.ApiHttpXml;
import karixTA.support.ExtentReport;
import karixTA.support.LoungeLib;
import karixTA.support.RestAssured;

public class TestTC_API_010VerifyXMLPlainMessageForGreaterThan160Characters extends ApiHttpXml {
	@Test
	public void test() {
		LoungeLib loungelibObject = new LoungeLib();
		String Test_caseId="TC_API_010";
		loungelibObject.workBookName = "Api.xls";
		loungelibObject.workSheetName = "ApiData";
		loungelibObject.sNo = Test_caseId;
		loungelibObject.fetchTestData();
		testCaseID = "KRX-18";
		summary=testCaseID +" : Verify XML plain message for interface rejection";

		test = ExtentReport.extent.startTest(Test_caseId);
		try {
			//Create Object For RestAssured
			RestAssured restassuredObject = new RestAssured();
			
			//Post Xml Response
			message = incrementSteps() + "- Post Xml Response";
			restassuredObject.postXmlData(apiXMLUrl, loungelibObject.fileName);
			test.log(LogStatus.PASS, message + " -- executed successfully");
			
			//Get 19 digit mid 	 be generated for the request
			message = incrementSteps() + "- 19 digit mid should be generated for the request";
			String info = restassuredObject.getRequestIdFromPostXmlResponse("info","xml");
			test.log(LogStatus.PASS, message + " -- executed successfully");
			
			//Message should be rejected at the interface 
			message = incrementSteps() + "- Message should be rejected at the interface info equals "+loungelibObject.expectedResult;
			Assert.assertEquals(info, loungelibObject.expectedResult);
			test.log(LogStatus.PASS, message + " -- executed successfully");
		} catch (Exception e) {
			Assert.fail("Failed Test - VerifyXMLPlainMessageForGreaterThan160Characters");
		}
	}
}

package karixTA.AUT;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.LogStatus;

import karixTA.pages.VisualizeLinks;
import karixTA.support.ExtentReport;
import karixTA.support.LoungeLib;

public class TestTC_Lounge_007OneToManyCampaignWithVL extends VisualizeLinks{
	
	@Test
	public void testOneToManyCampaignWithVL(){
		
		LoungeLib loungeObject = new LoungeLib();
		loungeObject.workBookName = "Lounge.xls";
		loungeObject.workSheetName = "OneToMany";
		loungeObject.sNo = "TC_LOUNGE_007";
		loungeObject.fetchTestData();
		testCaseID = "KRX-7";
		summary=testCaseID +" : Sending one to many campaign with VL";
		
		test = ExtentReport.extent.startTest("TC_LOUNGE_007");
		try{
			
			//Login with Newly Created User
			message = incrementSteps() + "- Login to Lounge with new User UserName: "+newlyCreatedUser;
			Assert.assertTrue(loginToLounge(newlyCreatedUser ,newlyCreatedUserPassword) ,message + " -- Failed");
			test.log(LogStatus.PASS, message + " -- executed successfully");
			
			//Click Global One
			message = incrementSteps() + "- Click Global One";
			Assert.assertTrue(click(lnkGlobalOne), message + " -- Failed");
			test.log(LogStatus.PASS,message + " -- executed successfully");
			
			//Click Upload File To Send SMS
			message = incrementSteps() + "- Click Upload File To Send SMS";
			Assert.assertTrue(click(btnUploadAFileToSendSMS), message + " -- Failed");
			test.log(LogStatus.PASS,message + " -- executed successfully");
			
			//Select entries for one to many
			message = incrementSteps() + "- Sending One To Many with campaign";
			Assert.assertTrue(sendingOneToManyWithCampaign(loungeObject), message + " -- Failed");
			test.log(LogStatus.PASS,message + " -- executed successfully");
			
			//Verify Status And Status_flag from DB
			fetchExcelData(loungeObject, "TC_LOUNGE_007_2");
			message = incrementSteps() + "- Verify Status_flag and Status is equals to"+loungeObject.expectedResult ;
			Assert.assertTrue(verifyDBStatus(loungeObject, "status_flag,status"), message + " -- Failed");
			test.log(LogStatus.PASS, message + " -- executed successfully");
			
		
		}catch(Exception e){
			Assert.fail("Failed To Verify OneToMany Campaign With VL");
		}
	}

}

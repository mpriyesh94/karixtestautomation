package karixTA.AUT;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.LogStatus;

import karixTA.pages.OneToManyCampaign;
import karixTA.support.ExtentReport;
import karixTA.support.LoungeLib;

public class TestTC_Lounge_004ScheduleOneToManyCampaign extends OneToManyCampaign{

	@Test
	public void testScheduleOneToManyCampaign(){
		
		LoungeLib oneToManyObj = new LoungeLib();
		oneToManyObj.workBookName = "Lounge.xls";
		oneToManyObj.workSheetName = "OneToMany";
		oneToManyObj.sNo = "TC_LOUNGE_007";
		oneToManyObj.fetchTestData();
		testCaseID = "KRX-4";
		summary=testCaseID +" : Scheduling one to many campaign";
		
		test = ExtentReport.extent.startTest("TC_LOUNGE_004");
		try{
			//Login with Newly Created User
			message = incrementSteps() + "- Login to Lounge with new User UserName: "+newlyCreatedUser;
			Assert.assertTrue(loginToLounge(newlyCreatedUser ,newlyCreatedUserPassword) ,message + " -- Failed");
			test.log(LogStatus.PASS, message + " -- executed successfully");
			
			//Click Global One
			message = incrementSteps() + "- Click Global One";
			Assert.assertTrue(click(lnkGlobalOne), message + " -- Failed");
			test.log(LogStatus.PASS,message + " -- executed successfully");
			
			//Click Upload File To Send SMS
			message = incrementSteps() + "- Click Upload File To Send SMS Button";
			Assert.assertTrue(click(btnUploadAFileToSendSMS), message + " -- Failed");
			test.log(LogStatus.PASS,message + " -- executed successfully");
			
			//Select Campaign To Send Message
			message = incrementSteps() + "- Select Campaign To Send Message";
			Assert.assertTrue(selectCampaignToSendMessage(oneToManyObj), message + " -- Failed");
			test.log(LogStatus.PASS,message + " -- executed successfully");
			
			//Click Schedule For Later Button
			message = incrementSteps() + "- Click Schedule For Later Button";
			Assert.assertTrue(click(btnScheduleForLater), message + " -- Failed");
			test.log(LogStatus.PASS,message + " -- executed successfully");
			
			//click Confirm And Schedule
			Thread.sleep(2000);
			message = incrementSteps() + "- Click Confirm And Schedule";
			Assert.assertTrue(click(btnConfirmAndSchedule), message + " -- Failed");
			test.log(LogStatus.PASS,message + " -- executed successfully");
			
			//Click Ok Button
			message = incrementSteps() + "- Click Ok Button";
			Assert.assertTrue(click(btnOK), message + " -- Failed");
			test.log(LogStatus.PASS,message + " -- executed successfully");
			
			//Verify Message Sent Successfully or not in UI
			message = incrementSteps() + "- Verify Message Sent Successfully Or Not";
			Assert.assertTrue(verifyTextMessages(successText, "Successfully sent."), message + " -- Failed");
			test.log(LogStatus.PASS,message + " -- executed successfully");
			
			//Verify Status And Status_flag from DB
			message = incrementSteps() + "- Verify Status_flag and Status is equals to"+oneToManyObj.expectedResult ;
			Assert.assertTrue(verifyDBStatus(oneToManyObj, "status_flag,status"), message + " -- Failed");
			test.log(LogStatus.PASS, message + " -- executed successfully");
			
			//Verify Status And reason from DB
//			fetchExcelData(oneToManyObj, "TC_LOUNGE_003_1");
//			message = incrementSteps() + "- Verify reason and Status is equals to"+oneToManyObj.expectedResult ;
//			Assert.assertTrue(verifyDBStatus(oneToManyObj, "status,reason"), message + " -- Failed");
//			test.log(LogStatus.PASS, message + " -- executed successfully");
			
		}catch(Exception e){
			Assert.fail("Failed To Schedule OneToMany Campaign");
		}
	}
}

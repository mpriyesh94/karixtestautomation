package karixTA.AUT;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.LogStatus;

import karixTA.pages.CreateNewUser;
import karixTA.support.DatabaseActions;
import karixTA.support.ExtentReport;
import karixTA.support.LoungeLib;
import karixTA.support.PropertyReader;

public class TestTC_Lounge_001LoungeCreateNewUser extends CreateNewUser {

	@Test
	public void testCreateNewUser() {

		LoungeLib loungeObject = new LoungeLib();
		loungeObject.workBookName = "Lounge.xls";
		loungeObject.workSheetName = "CreateNewUser";
		loungeObject.sNo = "TC_LOUNGE_001";
		loungeObject.fetchTestData();
		testCaseID = "KRX-1";
		summary = testCaseID + " : Creating new user through lounge";

		test = ExtentReport.extent.startTest("TC_LOUNGE_001");
		try {
			// login to lounge
			message = incrementSteps() + "- Login to Lounge UserName: " + existingUserName;
			Assert.assertTrue(loginToLounge(existingUserName, existingUserPassword), message + " -- Failed");
			test.log(LogStatus.PASS, message + " -- executed successfully");

			// Click User Managemnet
			message = incrementSteps() + "- Click User Management";
			Assert.assertTrue(click(userManagement), message + " -- Failed");
			test.log(LogStatus.PASS, message + " -- executed successfully");
			
			// Create New User
			message = incrementSteps() + "- Create New User";
			Assert.assertTrue(createNewUser(loungeObject), message + " -- Failed");
			test.log(LogStatus.PASS, message + " -- executed successfully");

			// Created Object For Database to call database methods
			DatabaseActions db = new DatabaseActions();

			// Get Password From DB
			message = incrementSteps() + "- Get Password From DB";
			String[] password = db.fetchQuery("SELECT pass FROM custdb.Users WHERE uname =" + "'" + newUser + "'",
					"pass");
			test.log(LogStatus.PASS, message + "New User Created Successfully and the password is " + password[0]);

			// Get Password From DB
			message = incrementSteps() + "- Get esmeaddr From DB ";
			String[] esmeaddress = db.fetchQuery("SELECT esmeaddr FROM custdb.Users WHERE uname =" + "'" + newUser + "'", "esmeaddr");
			test.log(LogStatus.PASS, message + esmeaddress[0]);

			PropertyReader.writeProperty("newUser", newUser);

			PropertyReader.writeProperty("newUserPassword", password[0]);
			PropertyReader.writeProperty("newUserEsmeaddress", esmeaddress[0]);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}

package karixTA.AUT;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.LogStatus;

import karixTA.pages.OneToManyCampaign;
import karixTA.support.ExtentReport;
import karixTA.support.LoungeLib;

public class TestTC_Lounge_002LoginWithNewUser extends OneToManyCampaign{
	
	@Test
	public void  testLoginWithNewUser(){
		
		LoungeLib loungeObject = new LoungeLib();
		loungeObject.workBookName = "Lounge.xls";
		loungeObject.workSheetName = "LoginWithNewUser";
		loungeObject.sNo = "TC_LOUNGE_002";
		testCaseID = "KRX-2";
		summary=testCaseID +" : Login with newly created user";
		
		loungeObject.fetchTestData();
		
		test = ExtentReport.extent.startTest("TC_LOUNGE_002");
		try{
			//Login with Newly Created User
			message = incrementSteps() + "- Login to Lounge with new User UserName: "+newlyCreatedUser;
			Assert.assertTrue(loginToLounge(newlyCreatedUser ,newlyCreatedUserPassword) ,message + " -- Failed");
			test.log(LogStatus.PASS, message + " -- executed successfully");
			
		}catch(Exception e){
			Assert.fail("Failed To Login With New User");
		}
	}
	
	
}

package karixTA.AUT;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.LogStatus;

import karixTA.pages.OneToManyCampaign;
import karixTA.support.ExtentReport;
import karixTA.support.LoungeLib;

public class TestTC_Lounge_003OneToManyCampaign extends OneToManyCampaign{
	
	@Test
	public void  testOneToManyCampaign(){
		
		LoungeLib oneToManyObj = new LoungeLib();
		oneToManyObj.workBookName = "Lounge.xls";
		oneToManyObj.workSheetName = "OneToMany";
		oneToManyObj.sNo = "TC_LOUNGE_003";
		oneToManyObj.fetchTestData();
		testCaseID = "KRX-3";
		summary=testCaseID +" : Sending one to many campaign";
		
		test = ExtentReport.extent.startTest("TC_LOUNGE_003");
		try{
			
			//Login with Newly Created User
			message = incrementSteps() + "- Login to Lounge with new User UserName: "+newlyCreatedUser;
			Assert.assertTrue(loginToLounge(newlyCreatedUser ,newlyCreatedUserPassword) ,message + " -- Failed");
			test.log(LogStatus.PASS, message + " -- executed successfully");
			
			//Click Global One
			message = incrementSteps() + "- Click Global One";
			Assert.assertTrue(click(lnkGlobalOne), message + " -- Failed");
			test.log(LogStatus.PASS,message + " -- executed successfully");
			
			//Click Upload file To Send SMS
			message = incrementSteps() + "- Click Upload File To Send SMS";
			Assert.assertTrue(click(btnUploadAFileToSendSMS), message + " -- Failed");
			test.log(LogStatus.PASS,message + " -- executed successfully");
			
			//Select campaign To Send Message
			message = incrementSteps() + "- Select Campaign To Send Message";
			Assert.assertTrue(selectCampaignToSendMessage(oneToManyObj), message + " -- Failed");
			test.log(LogStatus.PASS,message + " -- executed successfully");
			
			//Click Send SMS Now Button
			message = incrementSteps() + "- Click Send SMS Now Button";
			Assert.assertTrue(click(btnSendSMSNow), message + " -- Failed");
			test.log(LogStatus.PASS,message + " -- executed successfully");
			
			//Verify Message Sent Successfully Or Not 
			message = incrementSteps() + "- Verify Message Sent Successfully Or Not";
			Assert.assertTrue(verifyTextMessages(successText, "Successfully sent."), message + " -- Failed");
			test.log(LogStatus.PASS,message + " -- executed successfully");
			
			//Verify Status_flag and status from DB
			Thread.sleep(5000);
			message = incrementSteps() + "- Verify Status_flag and Status is equals to"+oneToManyObj.expectedResult ;
			Assert.assertTrue(verifyDBStatus(oneToManyObj, "status_flag,status"), message + " -- Failed");
			test.log(LogStatus.PASS, message + " -- executed successfully");
			
			//Verify status and reason from DB
//			fetchExcelData(oneToManyObj, "TC_LOUNGE_003_1");
//			message = incrementSteps() + "- Verify status and reason is equals to"+oneToManyObj.expectedResult ;
//			Assert.assertTrue(verifyDBStatus(oneToManyObj, "status,reason"), message + " -- Failed");
//			test.log(LogStatus.PASS, message + " -- executed successfully");
			
		}catch(Exception e){
			Assert.fail("Failed To Verfy One To Many Campaign");
		}
	}
	
	
}

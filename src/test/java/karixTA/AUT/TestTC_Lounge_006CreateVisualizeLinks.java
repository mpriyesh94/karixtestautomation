package karixTA.AUT;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.LogStatus;

import karixTA.pages.VisualizeLinks;
import karixTA.support.ExtentReport;
import karixTA.support.LoungeLib;

public class TestTC_Lounge_006CreateVisualizeLinks extends VisualizeLinks {

	@Test
	public void testCreateVisualizeLinks() {

		LoungeLib loungeObject = new LoungeLib();
		loungeObject.workBookName = "Lounge.xls";
		loungeObject.workSheetName = "VisualizeLinks";
		loungeObject.sNo = "TC_LOUNGE_006";
		loungeObject.fetchTestData();
		testCaseID = "KRX-6";
		summary = "KRX-26 : Verification of MM plain message";
		test = ExtentReport.extent.startTest("TC_LOUNGE_006");
		summary=testCaseID +" : Creating new VL in lounge";
		
		try {
			
			//Login with Newly Created User
			message = incrementSteps() + "- Login to Lounge with new User UserName: "+newlyCreatedUser;
			Assert.assertTrue(loginToLounge(newlyCreatedUser ,newlyCreatedUserPassword) ,message + " -- Failed");
			test.log(LogStatus.PASS, message + " -- executed successfully");

			// Click Visualize Link
			message = incrementSteps() + "- Click Visualize link";
			Assert.assertTrue(click(lnkVisualize), message + " -- Failed");
			test.log(LogStatus.PASS, message + " -- executed successfully");

			// Click Manage Visualize Link
			message = incrementSteps() + "- Click Manage Visualize link";
			Assert.assertTrue(click(lnkManageVisualizeLinks), message + " -- Failed");
			test.log(LogStatus.PASS, message + " -- executed successfully");

			// Click Create Visualize Link
			message = incrementSteps() + "- Click Create Visualize link";
			Assert.assertTrue(click(lnkCreateVisualizeLink), message + " -- Failed");
			test.log(LogStatus.PASS, message + " -- executed successfully");

			// Fill All Entries To Create Visualize Links
			message = incrementSteps() + "- Fill All Entries To Create Visualize Links";
			Assert.assertTrue(entriesForVisualizeLinks(loungeObject), message + " -- Failed");
			test.log(LogStatus.PASS, message + " -- executed successfully");

		} catch (Exception e) {
			e.printStackTrace();
			// Assert.fail("Failed To Create Visualize Links");
		}
	}

}

package karixTA.AUT;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.LogStatus;

import karixTA.pages.OneToManyCampaign;
import karixTA.support.ExtentReport;

public class TestTC_Lounge_005ReportVerification extends OneToManyCampaign {
	
	@Test
	public void testReportVerification(){
		testCaseID = "KRX-5";
		test = ExtentReport.extent.startTest("TC_LOUNGE_005");
		summary=testCaseID +" : Reports Verification";
		
		try{
			//Login with Newly Created User
			message = incrementSteps() + "- Login to Lounge with new User UserName: "+newlyCreatedUser;
			Assert.assertTrue(loginToLounge(newlyCreatedUser ,newlyCreatedUserPassword) ,message + " -- Failed");
			test.log(LogStatus.PASS, message + " -- executed successfully");
			
			//Click GlobalOne
			message = incrementSteps() + "- Click Global One";
			Assert.assertTrue(click(lnkGlobalOne), message + " -- Failed");
			test.log(LogStatus.PASS,message + " -- executed successfully");
			
			//Click Upload File To Send SMS
			message = incrementSteps() + "- Click Upload File To Send SMS Button";
			Assert.assertTrue(click(btnUploadAFileToSendSMS), message + " -- Failed");
			test.log(LogStatus.PASS,message + " -- executed successfully");
			
			//Click Report link
			message = incrementSteps() + "- Click Report";
			Assert.assertTrue(click(lnkReport), message + " -- Failed");
			test.log(LogStatus.PASS,message + " -- executed successfully");
			
			//Click Go
			message = incrementSteps() + "- Click Go";
			Assert.assertTrue(click(lnkGo), message + " -- Failed");
			test.log(LogStatus.PASS,message + " -- executed successfully");
			
			//Scroll Get Counts
			Thread.sleep(2000);
			message = incrementSteps() + "- Scroll The page";
			Assert.assertTrue(scroll(lnkGetCounts), message + " -- Failed");
			Assert.assertTrue(scroll(lnkGetCounts), message + " -- Failed");
			test.log(LogStatus.PASS,message + " -- executed successfully");
			
			//Click Get Counts to get Records
			Thread.sleep(2000);
			message = incrementSteps() + "- Click Get Counts";
			Assert.assertTrue(click(lnkGetCounts), message + " -- Failed");
			test.log(LogStatus.PASS,message + " -- executed successfully");
			
		}catch(Exception e){
			Assert.fail("Failed To Verify Report");
		}
	}

}

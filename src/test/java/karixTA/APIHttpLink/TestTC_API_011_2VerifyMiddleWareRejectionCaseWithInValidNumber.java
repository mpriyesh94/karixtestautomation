package karixTA.APIHttpLink;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.LogStatus;

import karixTA.support.ExtentReport;
import karixTA.support.LoungeLib;
import karixTA.support.RestAssured;
import karixTA.support.WebActions;

public class TestTC_API_011_2VerifyMiddleWareRejectionCaseWithInValidNumber extends WebActions{
	
	
	@Test
	public void testVerifyMiddleWareRejectionCaseWithInvalidNumber() {
		
		LoungeLib oneToManyObj = new LoungeLib();
		oneToManyObj.workBookName = "Api.xls";
		oneToManyObj.workSheetName = "APIQuery";
		oneToManyObj.sNo = "TC_API_011_2";
		oneToManyObj.fetchTestData();
		testCaseID = "KRX-23";
		summary=testCaseID +" : Verify middleware rejection case";
		
		test = ExtentReport.extent.startTest("TC_API_011_2");

		try {
			//Created Object To Call RestAssured Object
			RestAssured restassuredObject = new RestAssured();
			
			//Launch Webdriver
			message = incrementSteps() + "- Launch Driver";
			launchDriver();
			test.log(LogStatus.PASS,message+" Browser has launched Successfully");
			
			//Get API Base URL
			message = incrementSteps() + "- Get Api URL";
			getApiBaseUrl(oneToManyObj.apiURL);
			test.log(LogStatus.PASS,message+" Successfully get URL");
			
			//Get Request Id from getRequestIDFromURL
			message = incrementSteps() + "Get Request Id";
			String reqId = getRequestIDFromURL();
			test.log(LogStatus.PASS,message+" Successfully get Request Id From Browser "+reqId);
			
			//Get 19 digit mid should be generated for the request
			message = incrementSteps() + "- 19 digit mid should be generated for the request";
			String requestID = restassuredObject.getRequestIdFromPostXmlResponse(reqId,"URL");
			test.log(LogStatus.PASS, message + " -- executed successfully");
			
			//Verify Status and Status_flag from DB
			message = incrementSteps() + "- Verify Status_flag and Status is equals to"+oneToManyObj.expectedResult ;
			Assert.assertTrue(verifyApiResponseIntoDB(oneToManyObj, "status_flag,status",requestID), message + " -- Failed");
			test.log(LogStatus.PASS, message + " -- executed successfully");
			
		}catch(Exception e) {
			Assert.fail("Failed To Verify MiddleWare Rejection Case With Invalid Number");
		}
	}

}

package karixTA.APIHttpLink;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.LogStatus;

import karixTA.support.DatabaseActions;
import karixTA.support.ExtentReport;
import karixTA.support.LoungeLib;
import karixTA.support.RedisHelper;

public class TestTC_API_012VerifyAccountsAreLoadedProperlyInRedis extends RedisHelper{
	
	
	@Test
	public void testVerifyInvalidQSMessageGreaterThan160(){
		
		LoungeLib oneToManyObj = new LoungeLib();
		oneToManyObj.workBookName = "Api.xls";
		oneToManyObj.workSheetName = "APIQuery";
		oneToManyObj.sNo = "TC_API_012";
		oneToManyObj.fetchTestData();
		testCaseID = "KRX-25";
		summary=testCaseID +" : Verify accounts are loaded properly in master sync redis";
		
		test = ExtentReport.extent.startTest("TC_API_012");
		DatabaseActions db = new DatabaseActions();
		try {
			
			//Created Object For Database to call database methods			
			message = incrementSteps() + "Fetch Key Value From Database ";
			fetchExcelData(oneToManyObj, "TC_API_012_1");
			String[] keyValueBeforeUpdateInDB = db.fetchQuery(oneToManyObj.selectQuery,"aid");
			test.log(LogStatus.PASS, message +keyValueBeforeUpdateInDB[0]+ " -- executed successfully");
			
			fetchExcelData(oneToManyObj, "TC_API_012");
			message = incrementSteps() + "Updated Key Value ";
			db.executeQuery(oneToManyObj.executeQuery);
			test.log(LogStatus.PASS, message + " -- executed successfully");
		
			fetchExcelData(oneToManyObj, "TC_API_012_1");
			message = incrementSteps() + "Fetch Key Value From Database After Update";
			String[] keyValueAfterUpdateInDB = db.fetchQuery(oneToManyObj.selectQuery,oneToManyObj.key);
			test.log(LogStatus.PASS, message + " -- executed successfully");
			System.out.println(keyValueAfterUpdateInDB[0]);
			
			Thread.sleep(5000);
			message = incrementSteps() + "Fetch Key Value From Redis After Update";
			String keyValueInRedis = getRedisKeyValue(oneToManyObj.esmeAddress,"aid");
			test.log(LogStatus.PASS, message + " -- executed successfully");
			System.out.print(keyValueInRedis);
		
			message = incrementSteps() + "Verify Values Changed In DB Reflected In Redis ";
			Assert.assertEquals(keyValueAfterUpdateInDB[0], keyValueInRedis);
			test.log(LogStatus.PASS, message + "Value In Database :"+keyValueAfterUpdateInDB[0]+" Value In Redis : "+keyValueInRedis +"-- executed successfully");
		}catch(Exception e) {
			e.printStackTrace();
			
		}finally {
			message = incrementSteps() + "Updated Key Value ";
			db.executeQuery(oneToManyObj.executeQuery);
			test.log(LogStatus.PASS, message + " -- executed successfully");
		}
	}

}

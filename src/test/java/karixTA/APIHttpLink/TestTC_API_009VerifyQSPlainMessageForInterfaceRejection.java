package karixTA.APIHttpLink;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.LogStatus;

import karixTA.support.ExtentReport;
import karixTA.support.LoungeLib;
import karixTA.support.WebActions;

public class TestTC_API_009VerifyQSPlainMessageForInterfaceRejection extends WebActions{
	
	
	@Test
	public void testVerifyInvalidQSMessageGreaterThan160() {
		
		LoungeLib oneToManyObj = new LoungeLib();
		oneToManyObj.workBookName = "Api.xls";
		oneToManyObj.workSheetName = "APIQuery";
		oneToManyObj.sNo = "TC_API_009";
		oneToManyObj.fetchTestData();
		testCaseID = "KRX-21";
		summary=testCaseID +" : Verify QS plain message for interface rejection ";
		
		test = ExtentReport.extent.startTest("TC_API_009");
		try {
			//Launch Webdriver
			message = incrementSteps()+"Browser has launched Successfully";
			launchDriver();
			test.log(LogStatus.PASS,message+" -- executed successfully");
			
			//Get API Base URL
			message = incrementSteps()+"Launch API URL";
			getApiBaseUrl(oneToManyObj.apiURL);
			test.log(LogStatus.PASS,message+" -- executed successfully");
			
			//To get Content of Web Page
			message = incrementSteps()+"Successfully read the body content";
			String content = getContentUnderBodyTag();
			System.out.print(content);
			test.log(LogStatus.PASS,message+" -- executed successfully");
			
			//Get 19 digit mid should be generated for the request
			message = incrementSteps() + "- 19 digit mid should be generated for the request";
			Assert.assertTrue(content.contains("info=REJECTED"), message + " -- Failed");
			test.log(LogStatus.PASS, message + " -- executed successfully");
			
			
			
		}catch(Exception e) {
			Assert.fail("Failed To Verify Invalid QS Message Greater Than 160");
		}
	}

}

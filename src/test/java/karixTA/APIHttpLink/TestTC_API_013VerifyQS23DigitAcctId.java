package karixTA.APIHttpLink;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.LogStatus;

import karixTA.support.DatabaseActions;
import karixTA.support.ExtentReport;
import karixTA.support.LoungeLib;
import karixTA.support.RestAssured;
import karixTA.support.WebActions;

public class TestTC_API_013VerifyQS23DigitAcctId extends WebActions{
	
	
	@Test
	public void testVerifyInvalidQSMessageGreaterThan160() {
		
		LoungeLib oneToManyObj = new LoungeLib();
		oneToManyObj.workBookName = "Api.xls";
		oneToManyObj.workSheetName = "APIQuery";
		oneToManyObj.sNo = "TC_API_013";
		oneToManyObj.fetchTestData();
		testCaseID = "KRX-25";
		summary=testCaseID +" : Verify QS 23 digit ackid";
		
		test = ExtentReport.extent.startTest("TC_API_013");
		try {
			
			//Created Object To Call RestAssured Object
			RestAssured restassuredObject = new RestAssured();
			
			//Created Object For Database to call database methods
			DatabaseActions db = new DatabaseActions();
		
			//Updated The Query
			message = incrementSteps() + "Updated Account Configuration";
			db.executeQuery(oneToManyObj.executeQuery);
			test.log(LogStatus.PASS, message + " -- executed successfully");
			
			fetchExcelData(oneToManyObj, "TC_API_013_1");
			
			//Updated The Query
			message = incrementSteps() + "Updated User Configuration";
			db.executeQuery(oneToManyObj.executeQuery);
			test.log(LogStatus.PASS, message + " -- executed successfully");
			
			fetchExcelData(oneToManyObj, "TC_API_013");
			
			//Launch Webdriver
			message = incrementSteps()+"Browser has launched Successfully";
			launchDriver();
			test.log(LogStatus.PASS,message+" -- executed successfully");
			
			//Get API Base URL
			message = incrementSteps()+"Launch API URL";
			getApiBaseUrl(oneToManyObj.apiURL);
			test.log(LogStatus.PASS,message+" -- executed successfully");
			
			//Get Request Id from getRequestIDFromURL
			message = incrementSteps()+"Successfully get Request Id From Browser ";
			String reqId = getRequestIDFromURL();
			test.log(LogStatus.PASS,message+reqId+ " -- executed successfully");
			
			//Get 19 digit mid should be generated for the request
			message = incrementSteps() + "- 19 digit mid should be generated for the request";
			String requestID = restassuredObject.getRequestIdFromPostXmlResponse(reqId,"URL");
			test.log(LogStatus.PASS, message + " -- executed successfully");
			
			message="RequestId - " + requestID +" - is created at";
			fetchExcelData(oneToManyObj, "S_time");
			String S_Time=getsTime(oneToManyObj.selectQuery, requestID);
			test.log(LogStatus.PASS, message +" - "+ S_Time);
			
			fetchExcelData(oneToManyObj, "TC_API_013");
			//Verify Status and Status_flag from DB
			message = incrementSteps() + "- Verify Status_flag and Status is equals to"+oneToManyObj.expectedResult ;
			Assert.assertTrue(verifyApiResponseIntoDB(oneToManyObj, "status_flag,status",requestID), message + " -- Failed");
			test.log(LogStatus.PASS, message + " -- executed successfully");
			
			
			
		}catch(Exception e) {
			e.printStackTrace();
			//Assert.fail();
		}
	}

}

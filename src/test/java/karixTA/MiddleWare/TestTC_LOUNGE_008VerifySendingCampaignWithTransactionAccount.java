package karixTA.MiddleWare;


import org.testng.Assert;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.LogStatus;

import karixTA.pages.OneToManyCampaign;
import karixTA.support.DatabaseActions;
import karixTA.support.ExtentReport;
import karixTA.support.LoungeLib;

public class TestTC_LOUNGE_008VerifySendingCampaignWithTransactionAccount extends OneToManyCampaign {

	@Test
	public void testSendingCampaignWithTransactionAccount() {
		LoungeLib oneToManyObj = new LoungeLib();
		oneToManyObj.workBookName = "Lounge.xls";
		oneToManyObj.workSheetName = "OneToMany";
		oneToManyObj.sNo = "TC_LOUNGE_008";
		oneToManyObj.fetchTestData();
		testCaseID = "KRX-8";
		summary=testCaseID +" : Verification of sending campaign with transaction account with numeric senderid";
		
		test = ExtentReport.extent.startTest("TC_LOUNGE_008");
		try {
			
			//Created Object For Database to call database methods
			DatabaseActions db=new DatabaseActions();
			
			//Update Account Configuration
			message = incrementSteps() + "- Update Account Configuration";
			db.executeQuery(oneToManyObj.updateQuery);
			test.log(LogStatus.PASS, message + " -- executed successfully");
			
			//Update User Configuration
			message = incrementSteps() + "- Update User Configuration";
			fetchExcelData(oneToManyObj, "TC_LOUNGE_008_1");
			db.executeQuery(oneToManyObj.updateQuery);
			test.log(LogStatus.PASS, message + " -- executed successfully");
			
			//Delete Entries
			message = incrementSteps() + "- Delete from routedb.`govt_senderids` WHERE senderid = '123456'";
			fetchExcelData(oneToManyObj, "TC_LOUNGE_008");
			db.executeQuery(oneToManyObj.deleteQuery);
			test.log(LogStatus.PASS, message + " -- executed successfully");
			
			//Delete Entries
			message = incrementSteps() + "- DELETE FROM listdb.`global_senderid_block` WHERE senderid='123456'";
			fetchExcelData(oneToManyObj, "TC_LOUNGE_008_1");
			db.executeQuery(oneToManyObj.deleteQuery);
			test.log(LogStatus.PASS, message + " -- executed successfully");
			
			//Login with Newly Created User
			message = incrementSteps() + "- Login to Lounge with new User UserName: "+newlyCreatedUser;
			Assert.assertTrue(loginToLounge(newlyCreatedUser ,newlyCreatedUserPassword) ,message + " -- Failed");
			test.log(LogStatus.PASS, message + " -- executed successfully");
			
			//Switched To Global One Page
			message = incrementSteps() + "- Switched To Global One Page";
			Assert.assertTrue(click(lnkGlobalOne), message + " -- Failed");
			test.log(LogStatus.PASS, message + " -- executed successfully");

			//Switched To Global One Enterprise Messaging Page
			message = incrementSteps() + "- Switched To Global One Enterprise Messaging Page";
			Assert.assertTrue(click(btnUploadAFileToSendSMS), message + " -- Failed");
			test.log(LogStatus.PASS, message + " -- executed successfully");

			//Selected The Entries To Send Message
			message = incrementSteps() + "- Selected The Entries In Campaign using SenderID 'Alerts'";
			fetchExcelData(oneToManyObj, "TC_LOUNGE_008");
			Assert.assertTrue(selectCampaignToSendMessage(oneToManyObj), message + " -- Failed");
			test.log(LogStatus.PASS, message + " -- executed successfully");

			//Click SMS Send Now Button
			message = incrementSteps() + "- click Send Sms button";
			Assert.assertTrue(click(btnSendSMSNow), message + " -- Failed");
			test.log(LogStatus.PASS, message + " -- executed successfully");

			//Verify Message Sent Successfully or Not In UI
			message = incrementSteps() + "- Verify Message Sent message";
			Assert.assertTrue(verifyTextMessages(successText, "Successfully sent."), message + " -- Failed");
			test.log(LogStatus.PASS, message + " -- executed successfully");
			
			//Verify Status_flag and Status From DB
			message = incrementSteps() + "- Verify Status_flag and Status is equals to"+oneToManyObj.expectedResult ;
			Assert.assertTrue(verifyDBStatus(oneToManyObj, "status_flag,status"), message + " -- Failed");
			test.log(LogStatus.PASS, message + " -- executed successfully");
			
			//Verify Status And Reason from DB
//			fetchExcelData(oneToManyObj, "TC_LOUNGE_008_1");
//			message = incrementSteps() + "- Verify Status_flag and Status is equals to"+oneToManyObj.expectedResult ;
//			Assert.assertTrue(verifyDBStatus(oneToManyObj, "status,reason"), message + " -- Failed");
//			test.log(LogStatus.PASS, message + " -- executed successfully");
			
			
		} catch (Exception e) {
			Assert.fail("Cannot Verify Campaign With TransactionAccount");
		}
	}
}

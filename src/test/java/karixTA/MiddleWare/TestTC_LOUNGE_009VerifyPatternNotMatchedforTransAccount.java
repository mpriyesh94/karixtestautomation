package karixTA.MiddleWare;


import org.testng.Assert;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.LogStatus;

import karixTA.pages.OneToManyCampaign;
import karixTA.support.DatabaseActions;
import karixTA.support.ExtentReport;
import karixTA.support.LoungeLib;

public class TestTC_LOUNGE_009VerifyPatternNotMatchedforTransAccount extends OneToManyCampaign {

	@Test
	public void testVerifyPatternNotMatchedforTransAccount() {
		LoungeLib oneToManyObj = new LoungeLib();
		oneToManyObj.workBookName = "Lounge.xls";
		oneToManyObj.workSheetName = "OneToMany";
		oneToManyObj.sNo = "TC_LOUNGE_009";
		oneToManyObj.fetchTestData();
		testCaseID = "KRX-9";
		summary=testCaseID +" : Verification of pattern not matched for trans account and allowpromo is '0'";

		test = ExtentReport.extent.startTest("TC_LOUNGE_009");
		try {
			
			
			//Created Object For Database to call database methods
			DatabaseActions db = new DatabaseActions();
			
			message = incrementSteps() + "- Updated Account Configuration";
			db.executeQuery(oneToManyObj.updateQuery);
			test.log(LogStatus.PASS, message + " -- executed successfully");
			
			message = incrementSteps() + "- Updated User Configuration";
			fetchExcelData(oneToManyObj, "TC_LOUNGE_009_1");
			db.executeQuery(oneToManyObj.updateQuery);
			test.log(LogStatus.PASS, message + " -- executed successfully");
			
			message = incrementSteps() + "- Deleted .* Pattern";
			fetchExcelData(oneToManyObj, "TC_LOUNGE_009");
			db.executeQuery(oneToManyObj.deleteQuery);
			test.log(LogStatus.PASS, message + " -- executed successfully");
			
			//Login with Newly Created User
			message = incrementSteps() + "- Login to Lounge with new User UserName: "+newlyCreatedUser;
			Assert.assertTrue(loginToLounge(newlyCreatedUser ,newlyCreatedUserPassword) ,message + " -- Failed");
			test.log(LogStatus.PASS, message + " -- executed successfully");

			message = incrementSteps() + "- Switched To Global One Page";
			Assert.assertTrue(click(lnkGlobalOne), message + " -- Failed");
			test.log(LogStatus.PASS, message + " -- executed successfully");

			message = incrementSteps() + "- Switched To Global One Enterprise Messaging Page";
			Assert.assertTrue(click(btnUploadAFileToSendSMS), message + " -- Failed");
			test.log(LogStatus.PASS, message + " -- executed successfully");

			message = incrementSteps() + "- Selected The Entries In Campaign using SenderID '123456'";
			Assert.assertTrue(selectCampaignToSendMessage(oneToManyObj), message + " -- Failed");
			test.log(LogStatus.PASS, message + " -- executed successfully");

			message = incrementSteps() + "- click Send Sms button";
			Assert.assertTrue(click(btnSendSMSNow), message + " -- Failed");
			test.log(LogStatus.PASS, message + " -- executed successfully");

			message = incrementSteps() + "- Verify Message Sent message";
			Assert.assertTrue(verifyTextMessages(successText, "Successfully sent."), message + " -- Failed");
			test.log(LogStatus.PASS, message + " -- executed successfully");
			
			message = incrementSteps() + "- Verify Status_flag and Status is equals to"+oneToManyObj.expectedResult ;
			Assert.assertTrue(verifyDBStatus(oneToManyObj, "status_flag,status"), message + " -- Failed");
			test.log(LogStatus.PASS, message + " -- executed successfully");
			fetchExcelData(oneToManyObj, "TC_LOUNGE_009_1");
			
//			message = incrementSteps() + "- Verify reason and Status is equals to"+oneToManyObj.expectedResult ;
//			Assert.assertTrue(verifyDBStatus(oneToManyObj, "status,reason"), message + " -- Failed");
//			test.log(LogStatus.PASS, message + " -- executed successfully");
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			DatabaseActions db=new DatabaseActions();
			fetchExcelData(oneToManyObj, "TC_LOUNGE_009");
			db.executeQuery(oneToManyObj.executeQuery);
			
			fetchExcelData(oneToManyObj, "TC_LOUNGE_009_1");
			db.executeQuery(oneToManyObj.executeQuery);
			
			fetchExcelData(oneToManyObj, "TC_LOUNGE_009_2");
			db.executeQuery(oneToManyObj.executeQuery);
		}
	}
	
	
}

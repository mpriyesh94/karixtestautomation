package karixTA.MiddleWare;


import org.testng.Assert;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.LogStatus;

import karixTA.pages.OneToManyCampaign;
import karixTA.support.DatabaseActions;
import karixTA.support.ExtentReport;
import karixTA.support.LoungeLib;

public class TestTC_LOUNGE_010VerifyPatternMatchedforTransAccount extends OneToManyCampaign {

	@Test
	public void testVerifyPatternMatchedForTransAccount() {
		LoungeLib oneToManyObj = new LoungeLib();
		oneToManyObj.workBookName = "Lounge.xls";
		oneToManyObj.workSheetName = "OneToMany";
		oneToManyObj.sNo = "TC_LOUNGE_010";
		oneToManyObj.fetchTestData();
		testCaseID = "KRX-10";
		summary=testCaseID +" : Verification of pattern matched for trans account and allowpromo is '0'";
		test = ExtentReport.extent.startTest("TC_LOUNGE_010");
		try {
			
			//Created Object For Database to call database methods
			DatabaseActions db=new DatabaseActions();
			
			//Execute Update Query 1
			message = incrementSteps() + "- Updated Account Configuration";
			db.executeQuery(oneToManyObj.updateQuery);
			test.log(LogStatus.PASS, message + " -- executed successfully");
			
			//Execute Update Query 2
			message = incrementSteps() + "- Updated User Congiguration";
			fetchExcelData(oneToManyObj, "TC_LOUNGE_010_1");
			db.executeQuery(oneToManyObj.updateQuery);
			test.log(LogStatus.PASS, message + " -- executed successfully");
			
			//Execute Update Query 3
			message = incrementSteps() + "- Updated allow_Promo=1";
			fetchExcelData(oneToManyObj, "TC_LOUNGE_010_2");
			db.executeQuery(oneToManyObj.updateQuery);
			test.log(LogStatus.PASS, message + " -- executed successfully");
			
			//Execute Delete Query
			message = incrementSteps() + "- Insert .* Pattern";
			fetchExcelData(oneToManyObj, "TC_LOUNGE_010");
			db.executeQuery(oneToManyObj.updateQuery);
			test.log(LogStatus.PASS, message + " -- executed successfully");
			
			//Login with Newly Created User
			message = incrementSteps() + "- Login to Lounge with new User UserName: "+newlyCreatedUser;
			Assert.assertTrue(loginToLounge(newlyCreatedUser ,newlyCreatedUserPassword) ,message + " -- Failed");
			test.log(LogStatus.PASS, message + " -- executed successfully");
			
			//Switched To Global One Page
			message = incrementSteps() + "- Switched To Global One Page";
			Assert.assertTrue(click(lnkGlobalOne), message + " -- Failed");
			test.log(LogStatus.PASS, message + " -- executed successfully");

			//Click Upload File To Send SMS
			message = incrementSteps() + "- Switched To Global One Enterprise Messaging Page";
			Assert.assertTrue(click(btnUploadAFileToSendSMS), message + " -- Failed");
			test.log(LogStatus.PASS, message + " -- executed successfully");

			//Select Campaign To Send Message
			message = incrementSteps() + "- Selected The Entries In Campaign using SenderID '123456'";
			Assert.assertTrue(selectCampaignToSendMessage(oneToManyObj), message + " -- Failed");
			test.log(LogStatus.PASS, message + " -- executed successfully");

			//Click Send SMS Now
			message = incrementSteps() + "- click Send Sms button";
			Assert.assertTrue(click(btnSendSMSNow), message + " -- Failed");
			test.log(LogStatus.PASS, message + " -- executed successfully");

			//Verify Message Sent Successfull Message In UI
			message = incrementSteps() + "- Verify Message Sent message";
			Assert.assertTrue(verifyTextMessages(successText, "Successfully sent."), message + " -- Failed");
			test.log(LogStatus.PASS, message + " -- executed successfully");
			
			//Verify Status and Status_flag from DB
			message = incrementSteps() + "- Verify Status_flag and Status is equals to"+oneToManyObj.expectedResult ;
			Assert.assertTrue(verifyDBStatus(oneToManyObj, "status_flag,status"), message + " -- Failed");
			test.log(LogStatus.PASS, message + " -- executed successfully");
			
			//Verify reason and status from DB
			//fetchExcelData(oneToManyObj, "TC_LOUNGE_010_1");
//			message = incrementSteps() + "- Verify reason and Status is equals to"+oneToManyObj.expectedResult ;
//			Assert.assertTrue(verifyDBStatus(oneToManyObj, "status,reason"), message + " -- Failed");
//			test.log(LogStatus.PASS, message + " -- executed successfully");
			
			
		} catch (Exception e) {
			Assert.fail("Failed To Verify Pattern Matched For Trans Account");
		}
	}
}

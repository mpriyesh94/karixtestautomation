package karixTA.pages;

import java.util.Date;

import org.openqa.selenium.By;
import org.testng.Assert;

import com.sun.jna.platform.win32.Sspi.TimeStamp;

import karixTA.support.LoungeLib;
import karixTA.support.WebActions;

public class CreateNewUser extends WebActions{

	public By lnkLaunchOne = By.className("gotoGlobalone btLaunchNowBig");
	public By userManagement = By.xpath("//div[contains(text(),'User Management')]");
	public By createNew = By.linkText("Create New");
	public By rdoUser = By.id("user");
	public By rdoAdmin = By.id("admin");
	public By lnkCheckAvailability = By.linkText("Check availability");
	
	public By txtUserName = By.id("changeName");
	public By txtFirstName = By.id("firstName");
	public By txtEmail = By.id("email");
	public By txtMobileNumber = By.id("mob");
	
	public By lnkServices = By.linkText("Services");
	public By lnkSenderIDs = By.linkText("Sender IDs");
	public By btnCreateUser = By.id("createAdmin");
	public By btnSaveServices = By.className("saveButtonNew");
	public By btnAssign = By.id("assignSenderId");
	
	public By chkAlerts = By.xpath("//div[contains(text(),'Alerts')]/../div/input");
	public By chkEmail = By.xpath("//div[contains(text(),'EMail')]/input");
	public By chkVoice = By.xpath("//div[contains(text(),'Voice')]/input");
	public By chkSMS = By.xpath("//div[contains(text(),'SMS')]/input");
	public By userCreationSucessmessage = By.id("displayNotification");
	public By lnkAlreadyExists = By.xpath("//*[contains(text(),'Already Exists')]");

	public String serviceText = null;
	public String newUser;
	/**
	* This method used to Create New User
	* @return boolean true if is successfully executed, else false
	*/
	public boolean createNewUser(LoungeLib loungeObj){
		boolean returnValue = false;
		try{
			Date date= new Date();
			click(createNew);
			newUser = loungeObj.userName+date.getTime();
			System.out.println(newUser);
			type(txtUserName,newUser);
			click(lnkCheckAvailability);
			if(!isElementDisplayed(lnkAlreadyExists)){
			    type(txtFirstName,loungeObj.firstName);
				type(txtEmail,loungeObj.email_Id); //Email-ID
				type(txtMobileNumber,loungeObj.mobileNumber);
				click(lnkServices);
				selectServices(loungeObj.services);
				selectSenderId();
			}else{
				driver.navigate().back();
			}
			returnValue = true;
		}catch(Exception e){
			Assert.fail("Failed To Create New User");
		}
		return returnValue;
	}
	
	/**
	* This method used to Select Services
	* @param serviceName 
	* @return boolean true if is successfully executed, else false
	*/
	public boolean selectServices(String serviceName){
		boolean returnValue = false;
		try{
			click(chkEmail);
			click(chkVoice);
			String[] serviceName1 = serviceName.split(",");
			for(int i =0;i<serviceName1.length;i++) {
			click(By.xpath("//div[contains(text(),'"+serviceName1[i]+"')]/../div/input"));
			}
			click(btnSaveServices);
		}catch(Exception e){
			Assert.fail("Failed To Select Services");
		}
		return returnValue;
	}
	
	/**
	* This method used to Select SenderId
	* @return boolean true if is successfully executed, else false
	*/
	public boolean selectSenderId(){
		boolean returnValue = false;
		try{
			click(lnkSenderIDs);
			click(chkAlerts);
			click(btnAssign);
			click(btnCreateUser);
		}catch(Exception e){
			Assert.fail("Failed To Select Sender ID");
		}
		return returnValue;
	}
	}




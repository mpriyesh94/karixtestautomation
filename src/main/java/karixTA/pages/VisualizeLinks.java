package karixTA.pages;

import org.openqa.selenium.By;
import org.testng.Assert;
import karixTA.support.LoungeLib;


public class VisualizeLinks extends OneToManyCampaign{
	
	public By lnkVisualize = By.xpath("//div[contains(text(),'Visualize')]");
	public By lnkManageVisualizeLinks = By.id("sl_tab");
	public By lnkCreateVisualizeLink = By.id("crtSmartLnkBtn");
	public By txtCampaignName = By.id("campaign_name");
	public By campaignType = By.id("campaign_type");
	public By txtCampaignLink = By.id("campaign_link");
	public By btnSave = By.id("createBasicSL");
	public By visualizeLink = By.id("vlImage");
	public By txtVisualizeLinks = By.id("searchMessages2'");
	public By deleteLink = By.className("fa fa-trash");
	public By btnOk = By.id("okBtnCustoConfirmPopUp");
	public By btnPop_Ok = By.id("okBtnCustoPopUp");
	
	
	/**
	* This method used to Enter Value in Visualize links
	* @return boolean true if is successfully executed, else false
	*/
	public boolean entriesForVisualizeLinks(LoungeLib loungeObj){
		boolean returnValue = false;
		try{
			click(txtCampaignName);
			type(txtCampaignName,loungeObj.campaignName);
			selectFromDropDown(campaignType,loungeObj.campaignType);
			type(txtCampaignLink,loungeObj.campaignLink);
			click(btnSave);
			Thread.sleep(500);
			click(btnPop_Ok);
			returnValue = true;
		}catch(Exception e){
			Assert.fail("Failed To Enter Values In Visualize");
		}
		return returnValue;
	}
	
	/**
	 * 
	 * @param loungeObj
	 * @return
	 */
	public boolean sendingOneToManyWithCampaign(LoungeLib loungeObj){
		boolean returnValue = false;
		try{
			if(loungeObj.messageType.equalsIgnoreCase("Many To Many")){
				click(rdoManyToMany);
				
			}else if(loungeObj.messageType.equalsIgnoreCase("One To Many and/or paste numbers")){
				click(rdoOneToMany);
				uploadFile(btnUploadFile,loungeObj.fileName);
				click(txtOneToMany);				
				type(txtOneToMany,loungeObj.oneToManyNumber);
				waitForElementToVisible(txtTextArea);
				click(txtTextArea);
			click(visualizeLink);
			Thread.sleep(500);
			//click(txtVisualizeLinks);
			//type(txtVisualizeLinks,loungeObj.campaignLink);
			click(By.id("0"));
			//click(By.linkText("http//"+loungeObj.campaignLink));
			type(txtMessageTag,loungeObj.messageTag);
			selectFromDropDown(sendSMS,loungeObj.senderID);
			Thread.sleep(500);
			click(btnSendSMSNow);
			driver.navigate().back();
			driver.navigate().back();
			driver.navigate().back();
			}
			returnValue = true;
		}catch(Exception e){
			Assert.fail("Failed To Enter Values In Visualize");
		}
		return returnValue;
	}
	
	
	public boolean deleteCreatedLink() {
		boolean returnValue = false;
		try {
			
			click(lnkVisualize);
			click(lnkManageVisualizeLinks);
			click(deleteLink);
			click(btnOk);
			returnValue = true;
		}catch(Exception e) {
			Assert.fail("Cannot delete the link");
		}
		return returnValue;
	}
	

}

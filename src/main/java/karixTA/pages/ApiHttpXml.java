package karixTA.pages;


import org.testng.Assert;

import karixTA.support.DatabaseActions;
import karixTA.support.WebActions;

public class ApiHttpXml extends WebActions {
	DatabaseActions databaseactionsObject = new DatabaseActions();

	/**
	 * It is used to build query based on Request Id.
	 * @param getQuery
	 * @param id
	 * @return query
	 */
	public String queryForRequestId(String getQuery, String id) {
		String query = null;
		try {
			if (getQuery.contains("$1$")) {
				query = getQuery.replace("$1$", id);
			} else {
				query = getQuery;
			}
		} catch (Exception e) {
			Assert.fail("Could not get Request query " + getQuery);
		}
		return query;
	}

	/**
	 * It is used to check data affected into database or not.
	 * 
	 * @param query
	 * @param RequestId
	 * @return
	 */
	public boolean checkDataInsertedInDb(String query, String RequestId) {
		boolean returnValue = false;
		try {
			String sql = queryForRequestId(query, RequestId);

			databaseactionsObject.stmt.executeQuery(sql);
			returnValue = true;
		} catch (Exception e) {
			Assert.fail("Could not Execute query " + query);
		}
		return returnValue;
	}
	
	/**
	 * 
	 * @param query
	 * @param RequestId
	 * @return
	 */
	public boolean verifyUdhValue(String query, String RequestId,String columnName ) {
		boolean returnValue = false;
		try {
			String sql = queryForRequestId(query, RequestId);
			String[] data = databaseactionsObject.fetchQuery(sql, columnName);
			for (int val = 0; val < data.length; val++) {
				if (data[val] != null) {
					return true;
				} else {
					break;
				}
			}
			returnValue = true;
		} catch (Exception e) {
			Assert.fail("Could not Execute query " + query);
		}
		return returnValue;
	}

	
	/**
	 * It is used to check two rows inserted into db or not.
	 * @param query
	 * @param RequestId
	 * @param columnName
	 * @param expectedValue
	 * @return
	 */
	public boolean checkTwoRowsInsertedInDb(String query, String RequestId, String columnName,String expectedValue) {
		
		int counter = 2;
		boolean returnValue = false;

		while(counter > 0 && !returnValue ) {
			try {
					Thread.sleep(4000);
					String sql = queryForRequestId(query, RequestId);
					verifyDBStatusParseReqId(sql, columnName,expectedValue);
					returnValue = true;
			} catch (Exception e) {
				returnValue = false;
				//Assert.fail("Could not Execute query " + query);
			}
			counter--;
		}
		return returnValue;
	}
}

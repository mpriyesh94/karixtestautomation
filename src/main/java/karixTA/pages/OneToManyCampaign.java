package karixTA.pages;

import org.openqa.selenium.By;
import org.testng.Assert;

import karixTA.support.LoungeLib;
import karixTA.support.WebActions;

public class OneToManyCampaign extends WebActions {

	public By lnkGlobalOne = By.xpath("//div[contains(text(),'GlobalOne')]");
	public By btnUploadAFileToSendSMS = By.className("uploadFile");
	public By rdoManyToMany = By.id("manyToManyBttn");
	public By rdoOneToMany = By.id("oneToManyBttn");
	public By btnUploadFile = By.name("file");
	public By txtOneToMany = By.id("oneToManyTextArea");

	public By txtTextArea = By.id("textArea_1");
	public By txtMessageTag = By.id("oneToManyMessageTag");
	public By sendSMS = By.id("senderIDdropdownO2M");
	public By btnSendSMSNow = By.id("btn_send_o2m");
	public By successText = By.id("displaySentNotification");

	public By btnScheduleForLater = By.id("btn_schedule_o2m");
	public By btnConfirmAndSchedule = By.id("yes_reschedule_btn");
	public By btnOK = By.id("okBtnCustoConfirmPopUp");

	public By lnkReport = By.id("dlr_rept");
	public By lnkGo = By.id("ugo");
	public By percentage = By.className("tableR03");
	public By lnkGetCounts = By.xpath("//div[@id='download_country_count']//div[@class='btDownloadCounts']");

	/**
	 * 
	 * @param oneToManyObj
	 * @return
	 */
	public boolean selectCampaignToSendMessage(LoungeLib oneToManyObj) {
		boolean returnValue = false;
		try {
			System.out.println(oneToManyObj.messageType);
			if (oneToManyObj.messageType.equalsIgnoreCase("Many To Many")) {
				click(rdoManyToMany);
			} else if (oneToManyObj.messageType.equalsIgnoreCase("One To Many and/or paste numbers")) {
				click(rdoOneToMany);
				uploadFile(btnUploadFile, oneToManyObj.fileName);
				click(txtOneToMany);
				type(txtOneToMany, oneToManyObj.oneToManyNumber);
				waitForElementToVisible(txtTextArea);
				click(txtTextArea);
				type(txtTextArea, oneToManyObj.textMessage);
				type(txtMessageTag, oneToManyObj.messageTag);
				selectFromDropDown(sendSMS, oneToManyObj.senderID);
			}
			returnValue = true;
		} catch (Exception e) {
			Assert.fail("Failed in selectcampaignToSendMessage");
		}
		return returnValue;
	}

}

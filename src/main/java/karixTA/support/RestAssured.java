package karixTA.support;

import static io.restassured.RestAssured.given;

import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.testng.Assert;
import org.testng.annotations.Test;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import io.restassured.http.ContentType;
import io.restassured.response.Response;

public class RestAssured extends WebActions {
	public static String wikiString;
	public Response apiResponse;

	/**
	 * To serialize the xml file.
	 * 
	 * @param fileName
	 * @return
	 * @throws IOException
	 */
	public static String GenerateStringFromResource(String fileName) throws IOException {
		File dirl = new File(".");
		String file = dirl.getCanonicalPath() + File.separator + "src" + File.separator + "data" + File.separator
				+ "xml" + File.separator + fileName;
		Path wiki_path = Paths.get(file);
		byte[] wikiArray = Files.readAllBytes(wiki_path);

		return new String(wikiArray, "ISO-8859-1");

	}

	@Test
	private static Document convertStringToDocument(String xmlStr) {
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder;
		try {
			builder = factory.newDocumentBuilder();
			Document doc = builder.parse(new InputSource(new StringReader(xmlStr)));
			return doc;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Test
	/**
	 * To post xml data using rest assured.
	 * 
	 * @param ApiURL
	 * @param xmlFile
	 * @return boolean true if is successfully executed, else false.
	 * @throws IOException
	 */
	public boolean postXmlData(String ApiURL, String xmlFile) throws IOException {
		boolean getResponse = false;
		try {
			String myRequest = GenerateStringFromResource(xmlFile);
			Response response = given().contentType(ContentType.XML).accept(ContentType.XML).body(myRequest).when()
					.post(ApiURL);

			Assert.assertEquals(response.getStatusCode(), 200);
			System.out.println("POST Response\n" + response.asString());
			apiResponse = response;
			getResponse = true;
		} catch (AssertionError e) {
			Assert.fail("Invalid Xml Response");
		}
		return getResponse;
	}

	/**
	 * This method used to Fetch Specific Row Data in Excel Sheets.
	 * 
	 * @param data
	 *            instance of class to fetch test data from excel property file
	 * @param testCaseID
	 *            This is the id of current test case that is being executed
	 * @return boolean true if is successfully executed, else false.
	 * @throws ParserConfigurationException
	 * @throws IOException
	 * @throws SAXException
	 * @page Page:: generic
	 */
	public String getRequestIdFromPostXmlResponse(String getNodeValue, String requestType) {
		String getRequestID = null;
		String requestId = null;

		try {
			if (requestType.equalsIgnoreCase("xml")) {
				requestId = apiResponse.xmlPath().setRoot("a2wm.response").getString(getNodeValue);
			} else {
				requestId = getNodeValue;
			}
			int length = requestId.length();
			String req_id = requestId.substring(0, 4);
			if (req_id.equals("1111")) {
				getRequestID = requestId.substring(4, length);
			} else {
				getRequestID = requestId;
			}

		} catch (AssertionError e) {
			Assert.fail("Could not get Request ID From Post Xml Response Data");
		}
		return getRequestID;
	}

}
package karixTA.support;

import testlink.api.java.client.TestLinkAPIClient;
import testlink.api.java.client.TestLinkAPIResults;

public class TestLinkConnector {
	
	public static String testLinkURL = PropertyReader.readProperty("testlink_url");
	public static String testLinkDevKey = PropertyReader.readProperty("testlink_dev_key");
	public static String testLinkProjectName = PropertyReader.readProperty("testlink_project_name");
	public static String testLinkPlanName = PropertyReader.readProperty("testlink_plan_name");
	public static String testLinkBuildName = PropertyReader.readProperty("testlink_build_name");
	public static String testLinkSwitch = PropertyReader.readProperty("testlink_switch");
	
	public static void updateTestCaseStatus(String testCaseID, String testCaseStatus, String message) throws  testlink.api.java.client.TestLinkAPIException{
		try {

			TestLinkAPIClient testlinkAPIClient = new TestLinkAPIClient(testLinkDevKey, testLinkURL);
			if (testCaseStatus.equalsIgnoreCase("PASS")) {
				testlinkAPIClient.reportTestCaseResult(testLinkProjectName, testLinkPlanName, testCaseID, testLinkBuildName, message, TestLinkAPIResults.TEST_PASSED);
			} else {
				testlinkAPIClient.reportTestCaseResult(testLinkProjectName, testLinkPlanName, testCaseID, testLinkBuildName, message, TestLinkAPIResults.TEST_FAILED);
			}
		} catch (Exception e) {
			System.out.println("Error occured while updating testcase execution status to TestLink");
			e.printStackTrace();
		}
	}

}
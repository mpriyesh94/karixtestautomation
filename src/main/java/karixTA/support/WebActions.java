package karixTA.support;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import org.openqa.selenium.JavascriptExecutor;

public class WebActions {

	// creating object for Extent Report
	ExtentReport extentReportObject = new ExtentReport();

	public static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH-mm");
	// Config Properties
	public String browser = PropertyReader.readProperty("browser");
	public static WebDriver driver = null;
	public String previousUserName = null;
	public static String file = null;

	// Environment
	public String url = PropertyReader.readProperty("lounge_url");

	// New User Credentials
	public String newUserName = PropertyReader.readProperty("lounge_new_username");
	public String newUserPassword = PropertyReader.readProperty("lounge_new_password");

	// Existing USer Credentials
	public String existingUserName = PropertyReader.readProperty("lounge_existing_username");
	public String existingUserPassword = PropertyReader.readProperty("lounge_existing_password");
	public String apiBaseUrl = PropertyReader.readProperty("api_base_url");

	public String newlyCreatedUserEsmeddress = PropertyReader.getPropertyFile("user", "newUserEsmeaddress");
	public String newlyCreatedUser = PropertyReader.getPropertyFile("user", "newUser");
	public String newlyCreatedUserPassword = PropertyReader.getPropertyFile("user", "newUserPassword");

	public String apiXMLUrl = apiBaseUrl + PropertyReader.readProperty("api_xml_url");
	public boolean hasLoggedIn = false;

	// JIRA SWITCH
	public String JIRA_SWITCH = PropertyReader.readProperty("jira_switch");
	public String newlyCreatedUserName, newlyCreatedpassword;

	// After method
	public ExtentTest test;
	public String message = "";
	public int stepNum = 1;
	public String testCaseName;
	public String testRailId;

	public int test_runId;
	public String testCaseID;
	public String summary;

	public String Case_id;
	public String testCaseStatus;

	// login actions
	public By txtUsername = By.id("u-name");
	public By txtPassword = By.id("pswd");
	public By btnSignin = By.id("signInButton");
	public By lnkSignout = By.linkText("Sign Out");

	// Create Object For DatabaseActions
	DatabaseActions databaseactionsObject = new DatabaseActions();

	@BeforeSuite
	public void beforeSuite() throws IOException {
		extentReportObject.publishReports();

	}

	public String incrementSteps() {
		return "Step No: " + stepNum++;
	}

	@AfterSuite
	public void afterSuite() {
		if (driver != null) {
			driver.quit();
		}
		ExtentReport.extent.flush();
	}

	@AfterMethod
	public void getResult(ITestResult result) {
		try {

			// Logout post pass or fail
			if (driver != null) {
				if (isElementDisplayed(lnkSignout)) {
					click(lnkSignout);
				}
			}

			// creating object for ScreenShotCapture
			ScreenShotCapture screenshotObject = new ScreenShotCapture();
			if (result.getStatus() == ITestResult.FAILURE) {
				test.log(LogStatus.FAIL, message + "- FAIL");

				// Logging into JIRA for Failure

				test.log(LogStatus.FAIL, result.getThrowable());
				if (driver != null) {
					String screenShotPath = screenshotObject.capture(driver, generateFileName(result));
					test.log(LogStatus.FAIL,
							"Snapshot below: " + result.getMethod() + test.addScreenCapture(screenShotPath + ".png"));
				}

				testCaseStatus = "FAIL";
			} else {
				testCaseStatus = "PASS";
				test.log(LogStatus.PASS, "Test case Executed Successfully - PASS");
			}
			if (JIRA_SWITCH.trim().toLowerCase().equals("on")) {
				JiraConnector jiraConnector = new JiraConnector();
				jiraConnector.updateJiraIssue(summary, message);
			}

			// Logging into TestLink
			if (PropertyReader.readProperty("testlink_switch").equalsIgnoreCase("ON")) {
				TestLinkConnector.updateTestCaseStatus(testCaseID, testCaseStatus, message);
			}

			// Ending Extent Test
			ExtentReport.extent.endTest(test);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * This method is used to launch Webdriver.
	 */
	public void launchDriver() {
		File dir1 = new File(".");
		try {
			if (driver == null) {
				if (browser.trim().toLowerCase().equals("firefox")) {
					driver = new FirefoxDriver();
				} else if (browser.trim().toLowerCase().equals("chrome")) {
					System.setProperty("webdriver.chrome.driver",
							dir1.getCanonicalPath() + File.separator + "lib" + File.separator + "chromedriver.exe");
					driver = new ChromeDriver();
				}
			}
		} catch (Exception e) {
			Assert.fail("Could not launch Webdriver");
		}
	}

	/**
	 * This method is used to get url from config properties.
	 */
	public void getURL() {
		try {

			driver.get(url);

		} catch (Exception e) {
			Assert.fail("Could not launch URL");
		}
	}

	/**
	 * This method is used to get API Base url from config properties.
	 */
	public void getApiBaseUrl(String url) {
		try {

			driver.get(apiBaseUrl + url);

		} catch (Exception e) {
			Assert.fail("Could not launch API URL");
		}
	}

	/**
	 * This method is used to make a click actions.
	 * 
	 * @param by
	 * @return boolean true if is successfully executed, else false.
	 */
	public boolean click(By by) {
		boolean click = false;
		try {
			waitForElementToVisible(by);
			isElementClickable(by);
			driver.findElement(by).click();
			click = true;
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail("Could not click - " + by);
		}
		return click;
	}

	/**
	 * This method is used to check element is clickable or not.
	 * 
	 * @param by
	 * @return boolean true if is successfully executed, else false.
	 */
	public boolean isElementClickable(By by) {
		boolean isElementClickable = false;
		try {

			WebDriverWait wait = new WebDriverWait(driver, 10);
			wait.until(ExpectedConditions.elementToBeClickable(by));
			isElementClickable = true;
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail("Could not clickable - " + by);
		}
		return isElementClickable;
	}

	/**
	 * This method is used to scroll down the element.
	 * 
	 * @param by
	 * @return boolean true if is successfully executed, else false.
	 */
	public boolean scroll(By by) {
		boolean Scroll = false;
		try {

			waitForElementToVisible(by);
			WebElement scroll = driver.findElement(by);
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", scroll);
			Scroll = true;
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail("Could not Scroll - " + by);
		}
		return Scroll;
	}

	/**
	 * This method is used to type text in the textbox.
	 * 
	 * @param by
	 * @param textToType
	 * @return boolean true if is successfully executed, else false.
	 */
	public boolean type(By by, String textToType) {
		boolean returnValue = false;
		try {
			driver.findElement(by).clear();
			driver.findElement(by).sendKeys(textToType);
			returnValue = true;
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail("Could not Type - " + textToType);
		}
		return returnValue;
	}

	/**
	 * This method is used to get the text from specify path.
	 * 
	 * @param by
	 * @return boolean true if is successfully executed, else false.
	 */
	public String getText(By by) {
		String returnValue = "";
		try {
			returnValue = driver.findElement(by).getText();
		} catch (Exception e) {
			returnValue = "";
		}
		return returnValue;
	}

	/**
	 * This method is used to get textbox values.
	 * 
	 * @param by
	 * @return boolean true if is successfully executed, else false.
	 */
	public String getAttribute(By by) {
		String returnValue = "";
		try {
			returnValue = driver.findElement(by).getAttribute("value");
		} catch (Exception e) {
			returnValue = "";
		}
		return returnValue;
	}

	/**
	 * This method is used to close the alert box.
	 * 
	 * @return boolean true if is successfully executed, else false.
	 */
	public Boolean handleAlert() {
		// String message = null;
		boolean foundAlert = false;
		try {
			WebDriverWait wait = new WebDriverWait(driver, 100);
			wait.until(ExpectedConditions.alertIsPresent());
			Alert alert = driver.switchTo().alert();
			message = alert.getText();
			alert.accept();
			foundAlert = true;
		} catch (TimeoutException eTO) {
			foundAlert = false;
		}
		return foundAlert;
	}

	/**
	 * This method is used to switching between multiple windows
	 * 
	 * @param windowName
	 * @return boolean true if is successfully executed, else false.
	 */
	public boolean switchToNewWindow(String windowName) {
		boolean returnValue = false;
		try {
			Set<String> s = driver.getWindowHandles();
			Iterator<String> ite = s.iterator();
			int window = 1;
			while (ite.hasNext() && window < 10) {
				String popUpHandle = ite.next().toString();
				driver.switchTo().window(popUpHandle);
				if (driver.getTitle().equalsIgnoreCase(windowName)) {
					returnValue = true;
					break;
				}
				window++;
			}
		} catch (Exception e) {
			Assert.fail("Could Not Able To Switch to New Window");
		}
		return returnValue;
	}

	/**
	 * This method is used to switching between multiple windows and also verify
	 * elements on the new window.
	 * 
	 * @param windowName
	 * @param id
	 * @param text
	 * @return boolean true if is successfully executed, else false.
	 */
	public boolean switchToNewWindow(String windowName, By id, String text) {
		boolean returnValue = false;
		try {
			Set<String> s = driver.getWindowHandles();
			Iterator<String> ite = s.iterator();
			int window = 1;
			while (ite.hasNext() && window < 10) {
				String popUpHandle = ite.next().toString();
				driver.switchTo().window(popUpHandle);
				if (driver.getTitle().equalsIgnoreCase(windowName) && getText(id).equalsIgnoreCase(text)) {
					returnValue = true;
					break;
				}
				window++;
			}
		} catch (Exception e) {
			Assert.fail("Could Not Able To Switch to New Window");
		}
		return returnValue;
	}

	/**
	 * This method is used to select values from dropdown based on iteration.
	 * 
	 * @param textBox
	 * @param Value
	 * @return boolean true if is successfully executed, else false.
	 */
	public boolean selectFromDropDown(By textBox, String Value) {
		boolean DropDown = false;
		try {
			WebElement pro = driver.findElement(textBox);
			List<WebElement> options = pro.findElements(By.tagName("option"));
			for (int i = 0; i < options.size(); i++) {
				String val = options.get(i).getText().trim();
				if (val.equalsIgnoreCase(Value)) {
					options.get(i).click();
				}
			}

			DropDown = true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return DropDown;
	}

	/**
	 * This method is used to select dropdown based on visible text.
	 * 
	 * @param by
	 * @param textToSelect
	 * @return boolean true if is successfully executed, else false.
	 */
	public boolean selectByVisibleText(By by, String textToSelect) {
		boolean returnValue = false;
		try {
			Select select = new Select(driver.findElement(by));
			select.selectByVisibleText(textToSelect);
			returnValue = true;
		} catch (Exception e) {
			Assert.fail("Could not select - " + textToSelect);
		}
		return returnValue;
	}

	/**
	 * This method is used to select dropdown values based on index.
	 * 
	 * @param by
	 * @param indexToSelect
	 * @return boolean true if is successfully executed, else false.
	 */
	public boolean selectByIndex(By by, int indexToSelect) {
		boolean returnValue = false;
		try {
			Select select = new Select(driver.findElement(by));
			select.selectByIndex(indexToSelect);
			returnValue = true;
		} catch (Exception e) {
			Assert.fail("Could not select - " + indexToSelect);
		}
		return returnValue;
	}

	/**
	 * This method is used to check the element is present or not.
	 * 
	 * @param by
	 * @return boolean true if is successfully executed, else false.
	 */
	public boolean isElementPresent(By by) {

		boolean returnValue = false;
		try {
			driver.findElement(by);
			returnValue = true;
		} catch (Exception e) {
		}
		return returnValue;
	}

	/**
	 * This method is used to check the element is displayed or not.
	 * 
	 * @param by
	 * @return boolean true if is successfully executed, else false.
	 */
	public boolean isElementDisplayed(By by) {

		boolean returnValue = false;
		try {
			isElementPresent(by);
			returnValue = driver.findElement(by).isDisplayed();
		} catch (Exception e) {
		}
		return returnValue;
	}

	/**
	 * This method is used to clear the existing values in textbox.
	 * 
	 * @param by
	 */
	public void clear(By by) {
		try {
			driver.findElement(by).clear();
		} catch (Exception e) {
			Assert.fail("Could not clear the text box");
		}
	}

	public static void main(String args[]) {

		// System.out.println(readProperty("testrail_engine_url"));

	}

	/**
	 * This method is used to wait for the element to visible.
	 * 
	 * @param by
	 * @return boolean true if is successfully executed, else false.
	 */
	public boolean waitForElementToVisible(By by) {
		boolean returnValue = false;
		try {
			WebDriverWait wait = new WebDriverWait(driver, 250);
			wait.until(ExpectedConditions.visibilityOfElementLocated(by));
			returnValue = true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return returnValue;
	}

	/**
	 * To verify text messages is equals or not.
	 * 
	 * @param elementPath
	 * @param textValue
	 * @return
	 */
	public boolean verifyTextMessages(By elementPath, String textValue) {
		boolean returnValue = false;
		try {
			Assert.assertEquals(getText(elementPath), textValue);
			returnValue = true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return returnValue;
	}

	/**
	 * To verify text box messages is equals or not.
	 * 
	 * @param elementPath
	 * @param textValue
	 * @return
	 */
	public boolean verifyTextBoxMessages(By elementPath, String textValue) {
		boolean returnValue = false;
		try {
			Assert.assertEquals(getAttribute(elementPath), textValue);
			returnValue = true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return returnValue;
	}

	/**
	 * create generic file name.
	 * 
	 * @param result
	 * @return
	 */
	public static String generateFileName(ITestResult result) {

		Date date = new Date();
		String fileName = result.getName() + "_" + sdf.format(date);
		return fileName;

	}

	/**
	 * This method is used to Browse the file for upload
	 * 
	 * @param elementName
	 * @param fileName
	 * @return boolean true if successfully browsed the file
	 *
	 */

	public boolean uploadFile(By elementName, String fileName) {
		boolean returnValue = false;
		try {
			File dirl = new File(".");
			String uploadFile = dirl.getCanonicalPath() + File.separator + "src" + File.separator + "data"
					+ File.separator + "UploadFile" + File.separator;
			WebElement uploadElement = driver.findElement(elementName);
			// enter the file path onto the file-selection input field
			uploadElement.sendKeys(uploadFile + fileName);
			returnValue = true;
		} catch (Exception e) {
			Assert.fail("Could not Browse " + fileName);
			e.printStackTrace();
		}
		return returnValue;
	}

	/**
	 * This method is used to login to logue.
	 * 
	 * @param username
	 * @param password
	 * @return boolean true if is successfully executed, else false.
	 */
	public boolean loginToLounge(String username, String password) {
		boolean loginToLounge = false;
		try {
			launchDriver();
			getURL();
			driver.manage().window().maximize();
			driver.manage().timeouts().implicitlyWait(30000, TimeUnit.MILLISECONDS);
			type(txtUsername, username);
			type(txtPassword, password);
			click(btnSignin);
			Assert.assertEquals("Sign Out", getText(lnkSignout));
			loginToLounge = true;
		} catch (AssertionError e) {
			e.printStackTrace();
			Assert.fail("Could not login to the application");
		}
		return loginToLounge;
	}

	/**
	 * This method used to Fetch Specific Row Data in Excel Sheets.
	 * 
	 * @param data
	 *            instance of class to fetch test data from excel property file
	 * @param testCaseID
	 *            This is the id of current test case that is being executed
	 * @return boolean true if is successfully executed, else false.
	 * @page Page:: generic
	 */
	public boolean fetchExcelData(LoungeLib data, String testId) {
		boolean fetchXlData = false;
		try {
			data.sNo = testId;
			data.fetchTestData();
			fetchXlData = true;
		} catch (AssertionError e) {
			Assert.fail("Could not Fetch Excel Data");
		}
		return fetchXlData;
	}

	/**
	 * It is used to get sdate
	 * 
	 * @return
	 */
	public String getSdate() {
		String getDate = "";
		try {
			DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");
			LocalDateTime now = LocalDateTime.now();
			getDate = dtf.format(now);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return getDate;
	}

	/**
	 * It is used to verify data from database
	 * 
	 * @param oneToManyObj
	 * @param arrayList
	 * @return boolean true if is successfully executed, else false.
	 */
	public boolean verifyDBStatus(LoungeLib oneToManyObj, String arrayList) {
		boolean returnValue = false;
		try {
			String query = oneToManyObj.selectQuery + " esmeaddr = " + newlyCreatedUserEsmeddress + " AND sdate = '"
					+ getSdate() + "'";
			String[] data = databaseactionsObject.fetchQuery(query, arrayList);
			String[] actualValue = oneToManyObj.expectedResult.split(",");
			for (int val = 0; val < data.length; val++) {
				if (data[val] != null) {
					Assert.assertEquals(actualValue[val], data[val]);
				} else {
					break;
				}
			}
			returnValue = true;
		} catch (Exception e) {
			Assert.fail("Failed in selectcampaignToSendMessage");
		}
		return returnValue;
	}

	/**
	 * It is used to verify data from database basedOn RequestId
	 * 
	 * @param oneToManyObj
	 * @param arrayList
	 * @return boolean true if is successfully executed, else false.
	 */
	public boolean verifyDBStatusParseReqId(String Query, String colName, String expectedResult) {
		boolean returnValue = false;
		try {
			String[] data = databaseactionsObject.fetchQuery(Query, colName);
			String[] expectedValue = expectedResult.split(",");
			for (int val = 0; val < data.length; val++) {

				if (data[val] != null) {
					Assert.assertEquals(data[val], expectedValue[val]);
				} else {
					break;
				}
			}
			returnValue = true;
		} catch (Exception e) {
			Assert.fail("Failed in selectcampaignToSendMessage");
		}
		return returnValue;
	}

	/**
	 * To verify RestAssured Post XMl Response data into database.
	 * 
	 * @param oneToManyObj
	 * @param arrayList
	 * @param id
	 * @return boolean true if is successfully executed, else false.
	 */
	public boolean verifyApiResponseIntoDB(LoungeLib oneToManyObj, String dbColumnName, String id) {
		boolean returnValue = false;
		try {
			String query;
			String sql = oneToManyObj.selectQuery + " AND sdate = '" + getSdate() + "'";
			if (sql.contains("$1$")) {
				query = sql.replace("$1$", id);
			} else {
				query = oneToManyObj.selectQuery + " AND sdate = '" + getSdate() + "'";
			}

			String[] data = databaseactionsObject.fetchQuery(query, dbColumnName);
			String[] actualValue = oneToManyObj.expectedResult.split(",");
			for (int val = 0; val < data.length; val++) {
				if (data[val] != null) {
					System.out.print(actualValue[val] + "-" + data[val] + "\n");
					Assert.assertEquals(actualValue[val], data[val]);

				} else {
					break;
				}
			}
			returnValue = true;
		} catch (Exception e) {
			Assert.fail("Failed in selectcampaignToSendMessage");
		}
		return returnValue;
	}

	/**
	 * This method is used to get Request ID from URL
	 * 
	 * @return transactionId
	 */
	public String getRequestIDFromURL() {
		String transactionId = null;
		try {
			String content = getContentUnderBodyTag();
			String convertToString = content.toString();
			String[] reqId = convertToString.split("~");
			String[] Id = reqId[0].split("=");
			transactionId = Id[1];
			return transactionId;

		} catch (Exception e) {
			e.printStackTrace();
		}
		return transactionId;
	}

	/**
	 * This method is used to get Content from Browser
	 * 
	 * @return content
	 */
	public String getContentUnderBodyTag() {
		String content = null;
		try {
			content = driver.findElement(By.xpath("/html/body")).getText();
			return content;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return content;
	}

	/**
	 * It is used to get stime from db based on request Id
	 * 
	 * @param query
	 * @param stime
	 * @return
	 * @throws InterruptedException
	 */
	public String getsTime(String query, String reqId) throws InterruptedException {
		String getTime = "";
		String[] time = null;
		try {
			Thread.sleep(3000);
			if (reqId == null) {
				time = databaseactionsObject.fetchQuery(query, "stime");
			} else {
				time = databaseactionsObject.fetchQuery(query + reqId, "stime");
			}
			getTime = time[0];
		} catch (AssertionError e) {
			Assert.fail("Could not Fetch Excel Data");
		}
		return getTime;
	}
}

package karixTA.support;

import java.lang.reflect.Field;
import java.util.Hashtable;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;

public class LoungeLib {

	public String workSheetName = null;
	public String workBookName = null;

	public String sNo = null;
	public String userName = null;
	public String firstName = null;
	public String email_Id = null;
	public String mobileNumber = null;
	public String services = null;
	public String messageType = null;
	public String oneToManyNumber = null;
	public String textMessage = null;
	public String messageTag = null;
	public String senderID = null;
	public String campaignType = null;
	public String campaignName = null;
	public String campaignLink = null;
	public String fileName = null;
	public String insertQuery = null;
	public String selectQuery = null;
	public String newUserName = null;
	public String updateQuery = null;
	public String deleteQuery = null;
	public String expectedResult = null;
	public String apiURL = null;
	public String executeQuery = null;
	public String esmeAddress = null;
	public String key = null;

	public Hashtable<String, Integer> excelHeaders = new Hashtable<String, Integer>();
	public Hashtable<String, Integer> excelrRowColumnCount = new Hashtable<String, Integer>();

	public String toString() {
		StringBuffer listOfValues = new StringBuffer();
		@SuppressWarnings("rawtypes")
		Class cls = this.getClass();
		Field[] fields = cls.getDeclaredFields();
		Field field = null;
		try {
			for (int i = 0; i < fields.length; i++) {
				field = fields[i];
				Object subObj = field.get(this);
				if (subObj != null && !field.getName().equals("logger")) {
					listOfValues.append(":");
					listOfValues.append(field.getName());
					listOfValues.append("=");
					listOfValues.append(subObj.toString());
				}
			}
		} catch (RuntimeException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
		return listOfValues.toString();
	}

	public boolean fetchTestData() {

		ReadFromExcel readTestData = new ReadFromExcel();
		boolean isDataFound = false;
		try {

			HSSFSheet sheet = null;
			// function call to initiate a connection to an excel sheet
			sheet = readTestData.initiateExcelConnection("ExcelData", workSheetName, workBookName);

			// function to find number of rows and columns
			excelrRowColumnCount = readTestData.findRowColumnCount(sheet, excelrRowColumnCount);

			// function call to find excel header fields
			excelHeaders = readTestData.readExcelHeaders(sheet, excelHeaders, excelrRowColumnCount);

			HSSFRow row = null;
			HSSFCell cell = null;
			String tempId = null;

			for (int r = 0; r < excelrRowColumnCount.get("RowCount"); r++) {
				row = sheet.getRow(r);
				if (row != null) {
					for (int c = 0; c < excelrRowColumnCount.get("ColumnCount");) {
						cell = row.getCell(excelHeaders.get("S_No"));
						if (cell != null) {
							tempId = readTestData.convertHSSFCellToString(row.getCell(excelHeaders.get("S_No")));
							if (tempId.equals(sNo)) {
								isDataFound = true;
								if (workSheetName.equalsIgnoreCase("CreateNewUser")) {
									userName = readTestData
											.convertHSSFCellToString(row.getCell(excelHeaders.get("UserName")));
									firstName = readTestData
											.convertHSSFCellToString(row.getCell(excelHeaders.get("FirstName")));
									email_Id = readTestData
											.convertHSSFCellToString(row.getCell(excelHeaders.get("Email_Id")));
									mobileNumber = readTestData
											.convertHSSFCellToString(row.getCell(excelHeaders.get("MobileNumber")));
									services = readTestData
											.convertHSSFCellToString(row.getCell(excelHeaders.get("Services")));
									selectQuery = readTestData
											.convertHSSFCellToString(row.getCell(excelHeaders.get("SelectQuery")));
								} else if (workSheetName.equalsIgnoreCase("OneToMany")) {
									messageType = readTestData
											.convertHSSFCellToString(row.getCell(excelHeaders.get("MessageType")));
									oneToManyNumber = readTestData
											.convertHSSFCellToString(row.getCell(excelHeaders.get("OneToManyNumber")));
									textMessage = readTestData
											.convertHSSFCellToString(row.getCell(excelHeaders.get("TextMessage")));
									messageTag = readTestData
											.convertHSSFCellToString(row.getCell(excelHeaders.get("MessageTag")));
									senderID = readTestData
											.convertHSSFCellToString(row.getCell(excelHeaders.get("SenderID")));
									fileName = readTestData
											.convertHSSFCellToString(row.getCell(excelHeaders.get("FileName")));
									insertQuery = readTestData
											.convertHSSFCellToString(row.getCell(excelHeaders.get("InsertQuery")));
									selectQuery = readTestData
											.convertHSSFCellToString(row.getCell(excelHeaders.get("SelectQuery")));
									updateQuery = readTestData
											.convertHSSFCellToString(row.getCell(excelHeaders.get("UpdateQuery")));
									deleteQuery = readTestData
											.convertHSSFCellToString(row.getCell(excelHeaders.get("DeleteQuery")));
									expectedResult = readTestData
											.convertHSSFCellToString(row.getCell(excelHeaders.get("ExpectedResult")));
									campaignLink = readTestData
											.convertHSSFCellToString(row.getCell(excelHeaders.get("CampignLink")));
									executeQuery = readTestData
											.convertHSSFCellToString(row.getCell(excelHeaders.get("ExecuteQuery")));

								} else if (workSheetName.equalsIgnoreCase("VisualizeLinks")) {
									campaignName = readTestData
											.convertHSSFCellToString(row.getCell(excelHeaders.get("CampignName")));
									campaignType = readTestData
											.convertHSSFCellToString(row.getCell(excelHeaders.get("CampignType")));
									campaignLink = readTestData
											.convertHSSFCellToString(row.getCell(excelHeaders.get("CampignLink")));
									selectQuery = readTestData
											.convertHSSFCellToString(row.getCell(excelHeaders.get("SelectQuery")));
									expectedResult = readTestData
											.convertHSSFCellToString(row.getCell(excelHeaders.get("ExpectedResult")));

								} else if (workSheetName.equalsIgnoreCase("LoginWithNewUser")) {
									newUserName = readTestData
											.convertHSSFCellToString(row.getCell(excelHeaders.get("NewUserName")));
									selectQuery = readTestData
											.convertHSSFCellToString(row.getCell(excelHeaders.get("SelectQuery")));

								} else if (workSheetName.equalsIgnoreCase("APIQuery")) {
									apiURL = readTestData
											.convertHSSFCellToString(row.getCell(excelHeaders.get("ApiURL")));
									selectQuery = readTestData
											.convertHSSFCellToString(row.getCell(excelHeaders.get("SelectQuery")));
									expectedResult = readTestData
											.convertHSSFCellToString(row.getCell(excelHeaders.get("ExpectedResult")));
									executeQuery = readTestData
											.convertHSSFCellToString(row.getCell(excelHeaders.get("ExecuteQuery")));
									esmeAddress = readTestData
											.convertHSSFCellToString(row.getCell(excelHeaders.get("EsmeAddress")));
									key = readTestData
											.convertHSSFCellToString(row.getCell(excelHeaders.get("Key")));
								} else if (workSheetName.equalsIgnoreCase("OneToManyCampaignWithVL")) {
									messageType = readTestData
											.convertHSSFCellToString(row.getCell(excelHeaders.get("MessageType")));
									System.out.println(messageType);
									oneToManyNumber = readTestData
											.convertHSSFCellToString(row.getCell(excelHeaders.get("OneToManyNumber")));
									textMessage = readTestData
											.convertHSSFCellToString(row.getCell(excelHeaders.get("TextMessage")));
									messageTag = readTestData
											.convertHSSFCellToString(row.getCell(excelHeaders.get("MessageTag")));
									senderID = readTestData
											.convertHSSFCellToString(row.getCell(excelHeaders.get("SenderID")));
									fileName = readTestData
											.convertHSSFCellToString(row.getCell(excelHeaders.get("FileName")));
									campaignLink = readTestData
											.convertHSSFCellToString(row.getCell(excelHeaders.get("CampignLink")));
								} else if (workSheetName.equalsIgnoreCase("ApiData")) {
									fileName = readTestData
											.convertHSSFCellToString(row.getCell(excelHeaders.get("FileName")));
									insertQuery = readTestData
											.convertHSSFCellToString(row.getCell(excelHeaders.get("InsertQuery")));
									selectQuery = readTestData
											.convertHSSFCellToString(row.getCell(excelHeaders.get("SelectQuery")));
									updateQuery = readTestData
											.convertHSSFCellToString(row.getCell(excelHeaders.get("UpdateQuery")));
									deleteQuery = readTestData
											.convertHSSFCellToString(row.getCell(excelHeaders.get("DeleteQuery")));
									executeQuery = readTestData
											.convertHSSFCellToString(row.getCell(excelHeaders.get("ExecuteQuery")));
									expectedResult = readTestData
											.convertHSSFCellToString(row.getCell(excelHeaders.get("ExpectedResult")));

								}
								break;
							} else {
								break;
							}
						} else {
							break;
						}
					}
				}
				if (isDataFound) {
					break;
				}
			}
			if (!isDataFound) {
				System.out.println("No more data to read");
			}
		} catch (RuntimeException e) {
			e.printStackTrace();
		}
		return isDataFound;
	}

}

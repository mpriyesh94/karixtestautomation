package karixTA.support;

import redis.clients.jedis.Jedis;

public class RedisHelper extends WebActions{
	public String redisHost = "10.20.50.65";
	public int  redisPort = 6380;
	public int db = 2;
	String esmeNumber= "78924300000000";
	String key = "aid";	
	
	public String getRedisKeyValue(String esmeNumber, String key) {
		String redisVal = "";
		Jedis jedis = new Jedis(redisHost,redisPort);
		try {
			jedis.select(db);
			esmeNumber = esmeNumber.substring(1,esmeNumber.length());
			redisVal = jedis.hget("accbyesmeaddr:" +esmeNumber, key);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			jedis.close();
		}	
	  return redisVal;
	 }
}
package karixTA.support;

import net.rcarz.jiraclient.BasicCredentials;
import net.rcarz.jiraclient.Field;
import net.rcarz.jiraclient.Issue;
import net.rcarz.jiraclient.JiraClient;
import net.rcarz.jiraclient.JiraException;

import java.io.File;

public class JiraConnector {
	
	//Read the below Jira details from config file 
	public String jiraURL = "https://krxdemo.atlassian.net/";
	public String jiraUser = "priyesh.m@zucisystems.com";
	public String jiraPassword = "Zuci@123";
	public String jiraProject = "LNG";
	
	/**
	 * This method is used to update result in Jira.
	 * If any 'open' issue with provided 'summary' is found, comment is added with the message provided for the first issue
	 * else new issue is created with the 'summary' and 'description'
	 * 
	 *   @param summary -- Jira summary field value
	 *   @param message -- Description / comment value
	 */
	public String updateJiraIssue(String summary, String message) {
		
		try {
			Issue issue = createUpdateIssue(summary, message);
			return issue.getKey();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * This method is used to update result in Jira.
	 * If any 'open' issue with provided 'summary' is found, comment is added with the message provided for the first issue
	 * else new issue is created with the 'summary' and 'description'
	 * 
	 *   @param summary -- Jira summary field value
	 *   @param message -- Description / comment value
	 *   @param filePath -- attachment file path
	 */
	public String updateJiraIssueWithAttachment(String summary, String message, String filePath) {
		
		try {
			Issue issue = createUpdateIssue(summary, message);
			File attachFile = new File(filePath);
			issue.addAttachment(attachFile);
			return issue.getKey();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	
	/**
	 * This method is used to create / update result in Jira.
	 * If any 'open' issue with provided 'summary' is found, comment is added with the message provided for the first issue
	 * else new issue is created with the 'summary' and 'description'  
	 */
	Issue createUpdateIssue(String summary, String message) {
		try {
		BasicCredentials creds = new BasicCredentials (jiraUser, jiraPassword);
		JiraClient jiraClient = new JiraClient(jiraURL, creds);
		Issue.SearchResult searchResult = jiraClient.searchIssues("project = "+ jiraProject +" AND summary ~ \"" + summary + "\" AND status = Open ORDER BY created DESC");
		Issue issueID ;
		if (searchResult.total == 0) { 
			issueID = jiraClient.createIssue("LNG", "Bug").
					field(Field.SUMMARY, summary).
					field(Field.DESCRIPTION, message).
					execute();
			return issueID;
		}
		else {
			if (message == null || message.isEmpty()) message = "Adding failure comment"; 
			issueID = searchResult.issues.get(0);
			
			issueID.addComment(message);
			return issueID;
		}
		
		} catch (JiraException ex) {
			ex.printStackTrace();
	        System.err.println(ex.getMessage());
	        if (ex.getCause() != null)
	            System.err.println(ex.getCause().getMessage());
	    } catch (Exception e) {
	    	e.printStackTrace();
	    }
		return null;
	}

}
